package com.example.diceroller

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener { rollDice() }

        //inicio de la aplicacion
        rollDice()
    }

    /**Creacion del objeto con 6 lados **/
    private fun rollDice() {
        val dice = Dice(6)
        val diceRoll = dice.roll()

        /**lo que se muestra en el texto al rotar el dado**/
        //ENCONTRAR IMAGENES
        val diceImage: ImageView = findViewById(R.id.imageView)
        //DETERMINAR LAS IMAGENES USASDAS EN LOS MOVIMIENTOS DEL DADO
        val drawableResource = when (diceRoll) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        //ACTUALIZACION DEL IMAGEVIEW CON LA CORRECTA IMAGEN
        diceImage.setImageResource(drawableResource)
        //ACTUALIZAR LA DESCRIPCION DEL CONTENIDO
        diceImage.contentDescription = diceRoll.toString()
    }
}

class Dice(private val numSides: Int) {

    fun roll(): Int {
        return (1..numSides).random()
    }
}